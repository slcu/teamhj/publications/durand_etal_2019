### Introduction

This repository holds files for the analysis scripts and model simulations for the manuscript

<i>Cytoskeletal organization in isolated plant cells under geometry control</i><br>
P. Durand-Smet, Tamsin A. Spelman, E. M. Meyerowitz, H. Jönsson
https://doi.org/10.1073/pnas.2003184117


### Data

All original confocal imaging stacks are provided via the University of Cambridge Open Data Repository

https://doi.org/10.17863/CAM.51754

### This repository

The main files provided in this repository are: 

*  [/code/design](https://gitlab.com/slcu/teamhj/publications/durand_etal_2019/tree/master/code/design): CAD files specifying the designs of the microfluidic wells used in this study.
*  [/code/analysis](https://gitlab.com/slcu/teamhj/publications/durand_etal_2019/tree/master/code/analysis): scripts for analysing cytoskeletal angles and anisotropies,
running statistical tests, and plotting the results.
*  [/code/simulation](https://gitlab.com/slcu/teamhj/publications/durand_etal_2019/tree/master/code/simulation): configuration files for running simulations, extracting statistics on MT angles and anisotropies from the simulations, and plotting the results.


### Data analysis

An initial step, including manual marking of cell regions in the projected confocal stacks, using the [ImageJ](https://imagej.net) plugin [FibrilTool](https://www.plant-image-analysis.org/software/fibriltool), is performed to produce
anisotropy statistics from the data. The output of this analysis is stored in *.cvs files, and these files and the scripts used for further analysis can be found in
the [code/analysis/](https://gitlab.com/slcu/teamhj/publications/durand_etal_2019/tree/master/code/analysis) directory, where the Readme file provide further information.


### MT dynamics simulations

All simulations have been done using the [Tubulaton](https://gitlab.com/slcu/teamhj/tubulaton)
available via the Sainsbury Laboratory Gitlab repository. It is a c++-based software developed in a
linux environment. Installation instructions and Documentation is available via its [main page](https://gitlab.com/slcu/teamhj/tubulaton).

To run the specific simulations for generating data for this paper, parameter configuration files are provided in
the [code/simulations/](https://gitlab.com/slcu/teamhj/publications/durand_etal_2019/tree/master/code/simulations), and further instructions
can be found in the Readme file in this directory.


### Contact

For further information or questions, please contact

pauline.durand@slcu.cam.ac.uk<br>
henrik.jonsson@slcu.cam.ac.uk

