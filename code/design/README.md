### Files
The files contain the design of the shape used in the paper.
### Instructions
The files can be opened with AutoCAD (a free trial version is available here: https://www.autodesk.co.uk/products/autocad/overview).
### [Main repository](https://gitlab.com/slcu/teamhj/publications/durand_etal_2019)
